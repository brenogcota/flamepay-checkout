const express = require('express');
const cors = require('cors');
const app = express();

app.use(cors());

const products = [
    { id: 1, name: 'Camisa do Flamengo', description: 'Camisa oficial Clube de Regatas do Flamengo.', price: 250.99 },
    { id: 2, name: 'Jaqueta Calvin Klein', description: 'Jaqueta preta Kalvin Klein original tecido grosso.', price: 200.99 },
    { id: 3, name: 'Camisa do Cruzeiro', description: 'Camisa oficial Clube de Regatas do Cruzeiro', price: 350.99 },
];

app.get('/api/products', (req, res) => {
    res.json(products);
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});