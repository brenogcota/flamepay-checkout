export type Product = {
    id: String
    name: String
    description: String
    price: number
}