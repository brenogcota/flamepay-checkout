async function httpRequest(endpoint: string): Promise<any> {
    try {
        const response = await fetch('http://localhost:3000' + endpoint);
        if (!response.ok) {
            throw new Error('Failed to fetch');
        }
        const data: any[] = await response.json();
        return data;
    } catch (error) {
        console.error(error);
    }
}

export default httpRequest