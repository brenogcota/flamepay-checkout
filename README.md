# FlamePay Checkout

### DESAFIO FRONT-END FLAME PAY

Proposta do desafio: Analisar e implementar uma interface de um checkout de compras com dados fictícios, mas trabalhando com informações persistentes durante todos os processos do checkout. Você deverá criar uma barra lateral contendo um mock de produtos com Nome, Descrição e Preço, realizando o cálculo dinamicamente e exibindo no valor total, também demonstrado na barra lateral. Precisará criar um sistema de "steps" com as três etapas do checkout: Escolha do método de pagamento, Preenchimento dos dados (no caso de cartão de crédito) e página final.


![alt text](public/image.png)

### Tecnologias
- Vue 3
- Shadcn-vue
- Tailwindcss
- Express (server)

## Runing 
```
npm run dev
```

Server for mock products
```
cd backend && npm start
```